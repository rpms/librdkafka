Name:		librdkafka
Version:	1.6.1
Release:	1%{?dist}
Summary:	The Apache Kafka C library

Group:		Development/Libraries
License:	BSD
URL:		https://github.com/edenhill/librdkafka
Source0:	https://github.com/edenhill/librdkafka/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	python3
BuildRequires:	libzstd-devel
BuildRequires:	lz4-devel
BuildRequires:	openssl-devel
BuildRequires:	cyrus-sasl-devel
BuildRequires:	zlib-devel

Patch1: rsyslog-1.6.1-rhbz1842817-crypto-compliance.patch

%description
Librdkafka is a C/C++ library implementation of the Apache Kafka protocol,
containing both Producer and Consumer support.
It was designed with message delivery reliability and high performance in mind,
current figures exceed 800000 messages/second for the producer and 3 million
messages/second for the consumer.

%package	devel
Summary:	The Apache Kafka C library (Development Environment)
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
librdkafka is a C/C++ library implementation of the Apache Kafka protocol,
containing both Producer and Consumer support.
This package contains headers and libraries required to build applications
using librdkafka.

%prep
%setup -q

%patch -P 1 -p1

%build
%configure --enable-zlib \
           --enable-zstd \
           --enable-lz4 \
           --enable-lz4-ext \
           --enable-ssl \
           --enable-gssapi \
           --enable-sasl

%make_build

%check
make check

%install
%make_install
find %{buildroot} -name '*.a' -delete -print

%ldconfig_scriptlets

%files
%{_libdir}/librdkafka.so.*
%{_libdir}/librdkafka++.so.*
%doc README.md CONFIGURATION.md INTRODUCTION.md STATISTICS.md CHANGELOG.md LICENSE LICENSES.txt
%license LICENSE LICENSE.pycrc LICENSE.snappy LICENSES.txt

%files devel
%dir %{_includedir}/librdkafka
%attr(0644,root,root) %{_includedir}/librdkafka/*
%{_libdir}/librdkafka.so
%{_libdir}/librdkafka++.so
%{_libdir}/pkgconfig/rdkafka.pc
%{_libdir}/pkgconfig/rdkafka++.pc
%{_libdir}/pkgconfig/rdkafka-static.pc
%{_libdir}/pkgconfig/rdkafka++-static.pc

%changelog
* Tue Nov 14 2023 Attila Lakatos <alakatos@redhat.com> - 1.6.1-1
- Rebase to 1.6.1
  resolves: RHEL-12892
- Fix warnings reported by rpmlint
- Enable support for zlib/zstd compression and GSSAPI

* Mon Nov 01 2021 Attila Lakatos <alakatos@redhat.com> - 0.11.4-3
- Set SSL_CTX_set_cipher_list to use system-wide crypto policies
  resolves: rhbz#1842817

* Mon Jun 03 2019 Radovan Sroka <rsroka@redhat.com> - 0.11.4-2
- rebuild

* Fri Feb 08 2019 Jiri Vymazal <jvymazal@redhat.com> - 0.11.4-1
- rebase to v0.11.4 (0.11.5 was breaking rsyslog-kafka)
  resolves: rhbz#1614697

* Fri Aug 10 2018 Jiri Vymazal <jvymazal@redhat.com> - 0.11.5-1
- rebase to v0.11.5
  resolves: rhbz#1614697
- removed explicit attr macro on symlinks

* Thu Jun 28 2018 Radovan Sroka <rsroka@redhat.com> - 0.11.0-2
- switch from python2 to python3
  resolves: rhbz#1595795

* Thu Aug 31 2017 Michal Luscon <mluscon@gmail.com> - 0.11.0-1
- Update to 0.11.0

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon May 22 2017 Radovan Sroka <rsroka@redhat.com> - 0.9.5-1
- Update to 0.9.4

* Sat Mar 11 2017 Michal Luscon <mluscon@gmail.com> - 0.9.4-1
- Update to 0.9.4
- enable lz4, ssl, sasl

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild


* Fri Nov 11 2016 Radovan Sroka <rsroka@redhat.com> 0.9.2-1
- 0.9.2 release
- package created
